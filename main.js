function addFootPrintPNG(){
	if(this.value && this.footed !== "True" && this.type !== "password"){
		var loc = this.value.search("@");
		if(this.value.search("@") != -1){
			this.value = this.value.slice(0,loc).concat("+",window.location.hostname, this.value.slice(loc));
			this.footed = "True";
		}
	}
}

Array.from(document.getElementsByTagName('input')).forEach(function(s){s.onblur = addFootPrintPNG;});